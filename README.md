# Update Wireshark's Numbers

Update Wireshark's MAC address database, enterprise numbers, translations, author list, and other information.
Runs weekly.
